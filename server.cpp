#include "server.h"
#include <iostream>
#include <client.h>
#include <QUdpSocket>

#include <QMap>
#include <QVector>
#include <QDataStream>
#include <QTimer>
#include <QTcpServer>
#include <QtConcurrent>



using std::endl;
using std::cout;

Server::Server(QObject *parent) : QObject(parent)
{
    for(unsigned i =0;i<100;++i)
        for(unsigned k = 0;k<100;++k){
        cells.insert(QPair<unsigned,unsigned>(i,k),Cell());
        }
    server = new QTcpServer;
    udpsock = new QUdpSocket;

    connect(server,SIGNAL(newConnection()),this,SLOT(newConnection()));
    connect(server,SIGNAL(acceptError(QAbstractSocket::SocketError)),
            this,SLOT(acceptError(QAbstractSocket::SocketError)));
    if(!server->listen(QHostAddress::Any,7777)){
      cout <<"server not listen" << endl;
    }else{
       cout <<"server listen" << endl;
    }
    udpsendtimer = new QTimer(this);
    connect(udpsendtimer,SIGNAL(timeout()),this,SLOT(sendUDPInfos()));
    udpsendtimer->start(100);
    connect(this,SIGNAL(emitUDPBuf(QByteArray,quint32,qint16)),this,SLOT(sendUDPBuf(QByteArray,quint32,qint16)));
}

QString Server::command(QString c)
{
    QString tmpstr;
    if(c=="status"){
        tmpstr.append("players count:").append(QString::number(clients.size()));
    }
    return tmpstr;
}

void Server::removeEntity(unsigned cellX, unsigned cellY, Entity *t)
{
    auto iter =cells.find(QPair<unsigned,unsigned>(cellX,cellY));
    if(iter!=cells.end())iter->entites.removeOne(t);
}

void Server::appendEntity(unsigned cellX, unsigned cellY, Entity *t)
{
    auto iter =cells.find(QPair<unsigned,unsigned>(cellX,cellY));
    if(iter!=cells.end())iter->entites.append(t);
}

void Server::removeClient(Client *t)
{
    clients.removeOne(t);
}

void Server::sendUDPBuf(const QByteArray &buf,quint32 adress, qint16 port)
{
    udpsock->writeDatagram(buf,QHostAddress(adress),port);
}


void Server::sendUDPInfos()
{


    QtConcurrent::map(clients.begin(),clients.end(),[&](Client* a){
        if(!a->ball)return;
    QByteArray buf;
    QDataStream bufproxy(&buf,QIODevice::ReadWrite);
    bufproxy.setFloatingPointPrecision(QDataStream::SinglePrecision);
    bufproxy.setByteOrder(QDataStream::LittleEndian);
    QHostAddress adress = a->TcpSocket->peerAddress();
    quint16 port = a->TcpSocket->peerPort();
    unsigned X = floor(a->ball->getPos().x())/CellSize;
    unsigned Y = floor(a->ball->getPos().y())/CellSize;
    unsigned entCounter = 0;
    for(int i = -5;i<=5;++i)
        for(int j = -5;j<=5;++j){
             QMap<QPair<unsigned,unsigned>,Cell>::iterator iter =
                     cells.find(QPair<unsigned,unsigned>(X+i,Y+j));

            if(iter==cells.end()){
                continue;
            }
            for(auto ent: iter->entites){
                if(a->ball==ent)continue;
                bufproxy << (uint8_t)ent->getType()
                         << (uint32_t)ent->getID()
                         << ent->getPos()
                         <<ent->getVel()
                        << ent->getRot()
                         << ent->getVrot();
                entCounter++;

                if(entCounter>=8154){
                   emit emitUDPBuf(buf,adress.toIPv4Address(),port);
                   entCounter = 0;
                   buf.clear();
                }
            }
        }
    if(buf.size()>0)
        emit emitUDPBuf(buf,adress.toIPv4Address(),port);

    });

}

void Server::newConnection()
{

    Client* c = new Client(server->nextPendingConnection(),this);

    clients.append(c);
    cout <<"new client connected "<<std::hex << c <<endl;

}

void Server::acceptError(QAbstractSocket::SocketError socketError)
{
    Q_UNUSED(socketError)
    cout <<server->errorString().toStdString()<< endl;
}

