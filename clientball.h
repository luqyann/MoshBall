#ifndef CLIENTBALL_H
#define CLIENTBALL_H

#include <entity.h>

class Client;

class ClientBall : public Entity
{
    Q_OBJECT
public:
    ClientBall(Client* _client,
               const QVector3D &_pos,
               const QVector3D &_vel,
               const QVector4D &_rot,
               const QVector3D &_vrot,
               Server *_serv,
               QObject *parent = 0);

    ClientBall(Client* _client, Server *_serv, QObject *parent = 0);
    const Client* getClient()const{return client;}
private:
    Client* client;
};

#endif // CLIENTBALL_H
