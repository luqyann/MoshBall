QT += core network widgets concurrent
QT += gui

CONFIG += c++11

TARGET = MoshBallServer
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    server.cpp \
    client.cpp \
    entity.cpp \
    clientball.cpp

HEADERS += \
    server.h \
    client.h \
    entity.h \
    clientball.h

FORMS +=
