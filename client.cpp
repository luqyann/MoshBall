#include "client.h"
#include "entity.h"
#include <iostream>
#include "clientball.h"
#include <QDataStream>
#include "server.h"
#include <QTimer>


using std::endl;
using std::cout;

Client::Client(QTcpSocket* _TcpSocket, Server *_serv, QObject *parent) : QObject(parent),
    TcpSocket(_TcpSocket),
    server(_serv)
{
    TcpSocket->setSocketOption(QTcpSocket::LowDelayOption,1);
    connect(TcpSocket,SIGNAL(readyRead()),this,SLOT(readyRead()));
    connect(TcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),
            this,SLOT(error(QAbstractSocket::SocketError)));
    connect(TcpSocket,SIGNAL(disconnected()),this,SLOT(disconnected()));

    Send(InitToClient);
    QTimer::singleShot(5000,this,[&](){
        if(!bInited)disconnected();
    });


}

void Client::Send(Client::PocketKeyToClient key, const QVariant &a1, const QVariant &a2, const QVariant &a3)
{
    if(a1.type() == 0 ||
            a2.type() == 0||
            a3.type() == 0){}
    QByteArray buf;
    QDataStream bufproxy(&buf,QIODevice::ReadWrite);
    bufproxy.setByteOrder(QDataStream::LittleEndian);
    bufproxy.setFloatingPointPrecision(QDataStream::SinglePrecision);
    bufproxy << (uint8_t)key;
    switch(key)
    {
    case InitToClient:{
        break;
    }

    }
    TcpSocket->write(buf,buf.size());
}

void Client::readyRead()
{
    QByteArray buf = TcpSocket->readAll();
    QDataStream bufstream(buf);
    bufstream.setByteOrder(QDataStream::LittleEndian);
    bufstream.setFloatingPointPrecision(QDataStream::SinglePrecision);
    uint8_t key ;
    bufstream >> key;

    switch(key){
    case Client::PocketKeyToServer::InitToServer:{

        if(bInited)return;
        bInited = true;
        ball = new ClientBall(this,server,this);
        ball->SetType(ClientBall::Ball);
        QVector3D tmpPos;
        bufstream >> tmpPos >>ball->vel >> ball->rot >> ball->vrot;
        ball->setPos(tmpPos);
        break;
    }
    case Client::PocketKeyToServer::UpdateStates:{

        if(!ball || !bInited)return;
        QVector3D tmpPos;
        bufstream >> tmpPos >>ball->vel >> ball->rot >> ball->vrot;
        ball->setPos(tmpPos);
        break;
    }
    }
}


void Client::disconnected()
{
    delete ball;
    server->removeClient(this);
    TcpSocket->deleteLater();
    cout << "client " << std::hex << this << " disconnected" << endl;
    deleteLater();
}

void Client::error(QAbstractSocket::SocketError socketError)
{
    Q_UNUSED(socketError)
    cout << TcpSocket->errorString().toStdString() <<endl;
}

