#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QVector>
#include <entity.h>
#include <QTcpServer>


class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);
    QString command(QString c);
    void removeEntity(unsigned cellX,unsigned cellY,Entity* t);
    void appendEntity(unsigned cellX,unsigned cellY,Entity* t);
    void removeClient(class Client *t);

    static const int CellSize = 1000;
signals:
    emitUDPBuf(const QByteArray&,quint32,qint16);
private:
    class QTcpServer* server;
    QVector<class Client*> clients;

    struct Cell{

        QVector<Entity*> entites;
    };


    QMap<QPair<unsigned,unsigned>,Cell> cells;
    class QUdpSocket* udpsock;
    class QTimer* udpsendtimer;
public slots:

private slots:
    void sendUDPBuf(const QByteArray& buf,quint32 adress,qint16 port);
    void sendUDPInfos();
    void newConnection();
    void acceptError(QAbstractSocket::SocketError socketError);
};

#endif // SERVER_H
