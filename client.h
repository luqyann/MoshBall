#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(QTcpSocket* _TcpSocket,class Server* _serv,QObject *parent = 0);


    enum PocketKeyToClient:uint8_t{
        InitToClient = 0X00,

    };

    enum PocketKeyToServer:uint8_t{
        InitToServer =0x00,
        UpdateStates = 0x01
    };


    void Send(PocketKeyToClient key,const QVariant& a1 = 0,
                     const QVariant& a2 =0,
                     const QVariant& a3 =0);

signals:

public slots:

private slots:
    void readyRead();
    void disconnected();
    void error(QAbstractSocket::SocketError socketError);

private:
    friend class Server;
    class Server* server;
    QTcpSocket* TcpSocket;
    class Entity* ball = 0;
    bool bInited = false;
};

#endif // CLIENT_H
