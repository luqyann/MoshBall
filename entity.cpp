#include "entity.h"
#include "server.h"

Entity::Entity(const QVector3D &_pos, const QVector3D &_vel, const QVector4D &_rot, const QVector3D &_vrot, Server *_serv, QObject *parent) :
    QObject(parent),
    pos(_pos),
    vel(_vel),
    rot(_rot),
    vrot(_vrot),
    server(_serv)
{

}

Entity::Entity(class Server* _serv,QObject *parent):
    QObject(parent),
    server(_serv)
{

}

Entity::Entity(QObject *parent): QObject(parent){

}

Entity::~Entity()
{
    unsigned curX = floor(pos.x())/Server::CellSize;
    unsigned curY = floor(pos.y())/Server::CellSize;
    server->removeEntity(curX,curY,this);
}

void Entity::setPos(const QVector3D &_pos){
    unsigned curX = floor(pos.x())/Server::CellSize;
    unsigned curY = floor(pos.y())/Server::CellSize;
    unsigned X = floor(_pos.x())/Server::CellSize;
    unsigned Y = floor(_pos.y())/Server::CellSize;
    if(curX != X || curY!=Y){
        server->removeEntity(curX,curY,this);
        server->appendEntity(X,Y,this);


    }

    pos = _pos;
}
