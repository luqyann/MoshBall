#ifndef ENTITY_H
#define ENTITY_H

#include <QObject>
#include <QVector3D>
#include <QVector4D>
#include <QMap>

class Entity : public QObject
{
    Q_OBJECT
public:
    explicit Entity(const QVector3D& _pos,
                    const QVector3D& _vel,
                    const QVector4D& _rot,
                    const QVector3D& _vrot,
                    class Server* _serv,
                    QObject *parent = 0);

    explicit Entity(class Server* _serv,QObject *parent = 0);

    explicit Entity(QObject *parent = 0);
    ~Entity();

    enum EntityType : uint8_t {
        Ball = 0x00
    };

    void setPos(const QVector3D& _pos);

    inline void setVel(const QVector3D& _vel){vel = _vel;}
    inline void setRot(const QVector4D& _rot){rot = _rot;}
    inline void setVrot(const QVector3D& _vrot){vrot = _vrot;}
    inline void SetType(EntityType _type){type =_type;}

    inline const QVector3D& getPos()const{return pos;}
    inline const QVector3D& getVel()const{return vel;}
    inline const QVector4D& getRot()const{return rot;}
    inline const QVector3D& getVrot()const{return vrot;}
    inline uint32_t getID()const{return reinterpret_cast<uint32_t>(this);}
    inline EntityType getType()const{return type;}
private:
    friend class Client;
    EntityType type;
    QVector3D pos;
    QVector3D vel;
    QVector4D rot;
    QVector3D vrot;
    class Server* server;
signals:

public slots:


};

#endif // ENTITY_H
